﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Billetera.Negocio.Excepciones;
using Billetera.Entidades;
using Billetera.Entidades.Numeracion;
using Billetera.Entidades.Parametros;

namespace Billetera.Negocio.Impl
{
    public class ServiceTransaccion : IServiceTransaccion
    {
        public static List<Cuenta> _cuentas = new List<Cuenta>() {
            new Cuenta(){
                 CuentaId = 1,
                 Saldo = 1000,
                 Detalle = new List<Transaccion>()
                 {
                     new Transaccion(){
                          Cantidad = 1000,
                          Concepto = "Ingreso inicial",
                          Tipo = TipoTransaccion.Incremento
                     }
                 }
            }
        };
        public Cuenta GetCuenta (long cuentaId)
        {
            Cuenta cuenta=_cuentas.Where(x => x.CuentaId == cuentaId).FirstOrDefault();
            if (cuenta == null)
            {
                throw new CuentaNoEncontradaExcepcion(cuentaId);
            }
            return cuenta;
        }
        
        public Cuenta RegistroNuevaTransaccion(NuevaTransaccionParametros param)
        {
            Cuenta cuenta = GetCuenta(param.CuentaId);
            if (param.Cantidad <= 0)
            {
                throw new Exception("El monto debe ser mayor a 0.");
            }
            if (param.Tipo == TipoTransaccion.Incremento)
            {
                cuenta.Saldo += param.Cantidad;
            }
            else if (param.Tipo == TipoTransaccion.Decremento)
            {
                if (cuenta.Saldo - param.Cantidad < 0)
                {
                    throw new SaldoInsuficiente(param.CuentaId, cuenta.Saldo, param.Cantidad);
                }
                cuenta.Saldo -= param.Cantidad;
            }
            else
            {
                throw new Exception(string.Format("El tipo de transaccion {0} no existe.", param.Tipo));
            }
            cuenta.Detalle.Add(new Transaccion()
            {
                Cantidad = param.Cantidad,
                Concepto = param.Concepto,
                Tipo = param.Tipo
            });
            return cuenta;
        }
    }
}
