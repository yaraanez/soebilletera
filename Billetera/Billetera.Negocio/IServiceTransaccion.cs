﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Billetera.Entidades;
using Billetera.Entidades.Parametros;

namespace Billetera.Negocio
{
    public interface IServiceTransaccion
    {
        Cuenta GetCuenta(long cuentaid);
        Cuenta RegistroNuevaTransaccion(NuevaTransaccionParametros param);
    }
}
