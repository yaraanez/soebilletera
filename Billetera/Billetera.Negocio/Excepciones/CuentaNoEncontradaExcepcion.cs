﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billetera.Negocio.Excepciones
{
    public class CuentaNoEncontradaExcepcion : Exception
    {
        public CuentaNoEncontradaExcepcion(long cuentaId) : base(string.Format("La cuenta con identificador {0} no existe.", cuentaId.ToString()))
        {

        }
    }
}
