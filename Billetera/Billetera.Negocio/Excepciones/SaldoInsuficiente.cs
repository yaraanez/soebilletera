﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billetera.Negocio.Excepciones
{
   public class SaldoInsuficiente : Exception
    {
        public SaldoInsuficiente(
        long cuentaId,
            decimal saldo,
            decimal monto) : base(string.Format("La cuenta con identificador \"{0}\" no cuenta con el saldo suficiente \"{1}\" para descontar \"{2}\".", cuentaId.ToString(), saldo.ToString(), monto.ToString()))
        {

        }
    }
}
