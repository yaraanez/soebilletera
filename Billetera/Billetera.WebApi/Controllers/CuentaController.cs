﻿using Billetera.Entidades;
using Billetera.Entidades.Parametros;
using Billetera.Negocio;
using Billetera.Negocio.Impl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Billetera.WebApi.Controllers
{
    public class CuentaController : ApiController
    {
        private IServiceTransaccion _service;

        public CuentaController()
        {
            this._service = new ServiceTransaccion();
        }

        [HttpGet]
        public Cuenta Get(long id)
        {
            return this._service.GetCuenta(id);
        }

        [HttpPost]
        public Cuenta RegistrarTransaccion([FromBody]NuevaTransaccionParametros param)
        {
            return this._service.RegistroNuevaTransaccion(param);
        }
    }
}