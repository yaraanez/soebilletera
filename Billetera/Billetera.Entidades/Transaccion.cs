﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Billetera.Entidades.Numeracion;

namespace Billetera.Entidades
{
    public class Transaccion
    {
        public TipoTransaccion Tipo { get; set; }
        public decimal Cantidad { get; set; }
        public string Concepto { get; set; }
    }
}
