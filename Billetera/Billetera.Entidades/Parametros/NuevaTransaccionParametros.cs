﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Billetera.Entidades.Numeracion;
namespace Billetera.Entidades.Parametros
{
    public class NuevaTransaccionParametros
    {
        [Display(Name = "Numero de Cuenta")]
        public long CuentaId { get; set; }
        [Display(Name = "Tipo de Transacción")]
        public TipoTransaccion Tipo { get; set; }
        [Display(Name = "Monto")]
        public decimal Cantidad { get; set; }
        [Display(Name = "Concepto")]
        public string Concepto { get; set; }
    }
}
