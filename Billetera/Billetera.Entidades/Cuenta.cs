﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billetera.Entidades
{
   public class Cuenta
    {
        public long CuentaId { get; set; }
        public decimal Saldo { get; set; }
        public List<Transaccion> Detalle { get; set; }
    }
}
