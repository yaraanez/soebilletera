﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billetera.Entidades.Numeracion
{
    public enum TipoTransaccion
    {
        Incremento = 1,
        Decremento = 2
    }
}
