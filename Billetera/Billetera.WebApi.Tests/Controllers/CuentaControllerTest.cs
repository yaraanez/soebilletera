﻿using Billetera.Entidades;
using Billetera.Entidades.Numeracion;
using Billetera.Entidades.Parametros;
using Billetera.Negocio.Excepciones;
using Billetera.WebApi.Controllers;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Billetera.WebApi.Tests.Controllers
{
    [TestClass]
    public class CuentaControllerTest
    {
        [TestMethod]
        public void ObtenerCuentaExistente()
        {
            // Disponer
            CuentaController controlador = new CuentaController();

            try
            {
                long cuentaId = 1;
                // Actuar
                Cuenta result = controlador.Get(1);

                Assert.AreEqual(cuentaId, result.CuentaId);
            }
            catch (Exception ex)
            {
                Assert.Fail(ex.Message);
            }
        }

        [TestMethod]
        public void ExcepcionCuentaNoExiste()
        {
            // Disponer
            CuentaController controlador = new CuentaController();

            try
            {
                long cuentaId = 101;
                Cuenta account = controlador.Get(cuentaId);
                Assert.AreEqual(cuentaId, account.CuentaId);
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(CuentaNoEncontradaExcepcion));
            }
        }

        [TestMethod]
        public void TestRegisterIncreaseTransaction()
        {
            // Disponer
            CuentaController controlador = new CuentaController();

            NuevaTransaccionParametros parametro = new NuevaTransaccionParametros()
            {
                CuentaId = 1,
                Cantidad = 126,
                Tipo = TipoTransaccion.Incremento,
                Concepto = "Incremento"
            };
            Cuenta account = controlador.Get(parametro.CuentaId);
            decimal expectedAmount = account.Saldo + parametro.Cantidad;
            Cuenta response = controlador.RegistrarTransaccion(parametro);

            Assert.AreEqual(expectedAmount, response.Saldo);
        }

        [TestMethod]
        public void TestRegisterDecrementTransaction()
        {
            // Disponer
            CuentaController controlador = new CuentaController();

            NuevaTransaccionParametros parametro = new NuevaTransaccionParametros()
            {
                CuentaId = 1,
                Cantidad = 126,
                Tipo = TipoTransaccion.Decremento,
                Concepto = "Decremento"
            };
            Cuenta cuenta = controlador.Get(parametro.CuentaId);
            decimal expectedAmount = cuenta.Saldo - parametro.Cantidad;
            Cuenta response = controlador.RegistrarTransaccion(parametro);

            Assert.AreEqual(expectedAmount, response.Saldo);
        }

        [TestMethod]
        public void TestRegisterDecrementInsuficientAccountBalance()
        {
            // Disponer
            CuentaController controlador = new CuentaController();

            long cuentaId = 1;

            Cuenta account = controlador.Get(cuentaId);

            NuevaTransaccionParametros param = new NuevaTransaccionParametros()
            {
                CuentaId = cuentaId,
                Cantidad = account.Saldo + 10,
                Tipo = TipoTransaccion.Decremento,
                Concepto = "Salida"
            };
            try
            {
                Cuenta response = controlador.RegistrarTransaccion(param);
                Assert.Fail("Devio elevar la excepción, saldo insuficiente.");
            }
            catch (Exception ex)
            {
                Assert.IsInstanceOfType(ex, typeof(SaldoInsuficiente));
            }
        }
    }
}
