Feature: Mostrar información de una cuenta en función a su identificador
    Como un cliente de API WEB (no humano)
    
Scenario: Lectura de estado de cuenta.
    Given Los siguientes datos CuentaId: 1
    When Realizo un request GET hacia la url con el parametro ingresado     
    Then Recibo el estado de la cuenta con CuentaId: 1
    Then Recibo el estado de la cuenta con Saldo mayor o igual a 0
