Feature: Registrar transacción de incremento a una cuenta
    Como un cliente de API WEB (no humano)
    
Scenario: Saldo correcto luego de ingresar una transacción de incremento
    Given Los siguientes datos de nueva transacción son CuentaId: 1
    And Tipo: 1
    And Cantidad: 100
    And Concepto: "Ingreso recarga 100 Bs" 
    When Obtengo el Saldo actual de cuenta con CuentaId: 1
    And Realizo un request POST hacia la url con el parametro ingresado     
    Then Recibo el estado de la cuenta donde el nuevo Saldo es igual al Saldo actual + Cantidad 100