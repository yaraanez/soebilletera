Feature: Registrar transacción de incremento a una cuenta
    Como Usuario Final (humano)
    Quiero registrar una transcción a una cuenta

Scenario: Saldo correcto luego de ingresar una transacción de incremento
    Given Los siguientes datos de nueva transacción son CuentaId: 1
    And Tipo: 1
    And Cantidad: 100
    And Concepto: "Ingreso recarga 100 Bs" 
    And Obtengo el Saldo actual de cuenta con CuentaId: 1
    When Navego a la página principal
    And Llenar el campo CuentaId
    And Llenar el campo Tipo
    And Llenar el campo Cantidad
    And Llenar el campo Concepto
    And Hacer click en el boton Show
    Then Se debe mostrar el estado de la cuenta donde el nuevo Saldo es igual al Saldo actual + Cantidad

    