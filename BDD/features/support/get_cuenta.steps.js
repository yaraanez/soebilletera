const { Given, When, Then } = require("cucumber");
const { expect } = require("chai");

Given('Los siguientes datos CuentaId: {int}', function (cuentaId) {
    this.setId(cuentaId);
});

Given('UrlBase: {string}', function (urlBase) {
    this.setUrlBase(urlBase);
});

When('Realizo un request GET hacia la url con el parametro ingresado', async function () {
    await this.makeGetRequest();
});

Then('Recibo el estado de la cuenta con CuentaId: {int}', function (cuentaId) {
    expect(this.apiGetResponse.CuentaId).to.eql(cuentaId);
});

Then('Recibo el estado de la cuenta con Saldo mayor o igual a {int}', function (saldo) {
    expect(this.apiGetResponse.Saldo).to.gte(saldo);
});