const { Given, When, Then } = require('cucumber')
const { expect } = require('chai')
const { Builder, By, Key, until } = require('selenium-webdriver');

let chromeDriver = undefined;

When('Navego a la página principal', {timeout: 2 * 5000}, async function () {
    chromeDriver = await new Builder().forBrowser('chrome').build();    
    await chromeDriver.get('http://localhost/Billetera.WebApi/');
});

When('Llenar el campo CuentaId', async function () {
    await chromeDriver.findElement(By.id('CuentaId')).sendKeys(this.nuevaTransaccionParametros.CuentaId);
});

When('Llenar el campo Tipo', async function () {
    await chromeDriver.findElement(By.id('Tipo')).sendKeys(this.nuevaTransaccionParametros.Tipo);
});

When('Llenar el campo Cantidad', async function () {
    await chromeDriver.findElement(By.id('Cantidad')).sendKeys(this.nuevaTransaccionParametros.Cantidad);
});

When('Llenar el campo Concepto', async function () {
    await chromeDriver.findElement(By.id('Concepto')).sendKeys(this.nuevaTransaccionParametros.Concepto);
});

When('Hacer click en el boton Show', async function () {
    await chromeDriver.findElement(By.id('btnRegister')).click();
});

Then('Se debe mostrar el estado de la cuenta donde el nuevo Saldo es igual al Saldo actual + Cantidad', async function () {
    await chromeDriver.findElement(By.id('Saldo'))
        .getText().then(function (text) {
            showText = text;
        });

    let expected = parseInt(this.apiGetResponse.Saldo + this.nuevaTransaccionParametros.Cantidad);

    expect(expected).to.eq(parseInt(showText));
    await chromeDriver.quit();
});