const { setWorldConstructor } = require("cucumber");
const httpClient = require('request-promise');
class CuentaWord {
    constructor() {
        this.cuentaId = 0;
        this.nuevaTransaccionParametros = {
            CuentaId : 0,
            Tipo: 0,
            Cantidad: 0,
            Concepto: ''
        };
        this.urlBase = "http://localhost/Billetera.WebApi";
        this.httpOptions = {};
        this.apiGetResponse = {};
        this.apiPostResponse = {};
        this.getMethod = "/api/Cuenta/";
        this.postMethod = "/api/Cuenta";
    }

    setId(cuentaId) {
        this.cuentaId = cuentaId;
    }

    setUrlBase(urlBase) {
        this.urlBase = urlBase;
    }

    async makeGetRequest() {
        this.httpOptions = {
            method: 'GET',
            uri: `${this.urlBase}${this.getMethod}${this.cuentaId}`,
            json: true
        };

        await httpClient(this.httpOptions)
            .then((response) => {
                console.log(response);
                this.apiGetResponse = response;
            })
            .catch((error) => {
                console.log(error.error.ExceptionMessage ? error.error.ExceptionMessage : error.error);
                this.apiGetResponse = {};
            });
    }

    async makePostRequest() {
        var httpOptions = {
            method: 'POST',
            uri: `${this.urlBase}${this.postMethod}`,
            body: this.nuevaTransaccionParametros,
            json: true
        };

        await httpClient(httpOptions)
            .then((response) => {
                console.log(response);
                this.apiPostResponse = response;
            })
            .catch((error) => {
                console.log(error.error.ExceptionMessage ? error.error.ExceptionMessage : error.error);
                this.apiPostResponse = {};
            });
    }
}

setWorldConstructor(CuentaWord);
