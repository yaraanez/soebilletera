const { Given, When, Then } = require("cucumber");
const { expect } = require("chai");

Given('Los siguientes datos de nueva transacción son CuentaId: {int}', function (cuentaId) {
    this.nuevaTransaccionParametros.CuentaId = cuentaId;
});


Given('Tipo: {int}', function (tipo) {
    this.nuevaTransaccionParametros.Tipo = tipo;
});

Given('Cantidad: {int}', function (cantidad) {
    this.nuevaTransaccionParametros.Cantidad = cantidad;
});

Given('Concepto: {string}', function (concepto) {
    this.nuevaTransaccionParametros.Concepto = concepto;
});

When('Obtengo el Saldo actual de cuenta con CuentaId: {int}', async function (cuentaId) {
    this.setId(cuentaId);
    await this.makeGetRequest();
});

When('Realizo un request POST hacia la url con el parametro ingresado', async function () {
    await this.makePostRequest();
});

Then('Recibo el estado de la cuenta donde el nuevo Saldo es igual al Saldo actual + Cantidad {int}', function (cantidad) {
    expect(this.apiPostResponse.Saldo).to.eql(this.apiGetResponse.Saldo + cantidad);
});