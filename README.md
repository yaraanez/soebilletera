# BilleteraY

### 1 Debe asegurarse de tener iis configurado, y visual studio 2017 o superior

Abrir el proyecto `Billetera\Billetera.sln` con permisos de administrador.
Ejecutar las pruebas del proyecto `Billetera.WebApi.Tests`
Ejecutar el proyecto `Billetera.WebApi` para exponer el componente API.
Se debe poder visualizar el home del proyecto via la url http://localhost/Billetera.WebApi
Se debe poder visualizar la definición de los métodos via la url http://localhost/Billetera.WebApi/swagger

### 2 Abrir e instalar las dependencias del proyecto 'bdd'

Ejecute el siguiente comando:

  `npm install`

### 4 Ejecute las pruebas del proyecto 'bdd'

  `./node_modules/.bin/cucumber-js `